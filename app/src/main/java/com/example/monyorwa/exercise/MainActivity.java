package com.example.monyorwa.exercise;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void towindow1(View a)
    {
        Intent window1 = new Intent(MainActivity.this, Main2Activity.class);
        startActivity(window1);
    }

    public void towindow2(View a)
    {
        Intent window2 = new Intent(MainActivity.this, Main2Activity.class);
        startActivity(window2);
    }

    public void towindow3(View a) {
        Intent window3 = new Intent(MainActivity.this, Main2Activity.class);
        startActivity(window3);
    }
}
